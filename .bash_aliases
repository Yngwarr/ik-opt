# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias rg='rg -C3'
alias bc='bc -q'

alias pwd='ik-pwd'
