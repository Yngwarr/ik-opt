# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

PROMPT_COMMAND=__prompt_cmd
__prompt_cmd() {
	local EXIT="$?"

	# Terminal Control Escape Sequences: http://www.termsys.demon.co.uk/vtansi.htm
	local RED="\[\e[1;91m\]"
	local GREEN="\[\e[1;92m\]"
	local YELLOW="\[\e[1;93m\]"
	local BLUE="\[\e[1;94m\]"
	local CYAN="\[\e[1;96m\]"
	local DRED="\[\e[0;31m\]"
	local DCYAN="\[\e[0;36m\]"
	local RESET="\[\e[0m\]"

	# green arrow if the last command terminated successfully
	local SUCC_COL=$(if [[ ${UID} -eq "0" ]] ; then echo "${YELLOW}" ; else echo "${GREEN}" ; fi)
	local CODE=$(if [[ ${EXIT} == 0 ]] ; then echo "${SUCC_COL}> " ; else echo "${RED}> " ; fi)

	# it's handy to see if there are any stopped jobs
	local JOBS_COUNT=$(jobs | wc -l)
	local JOBS=$(if [[ ${JOBS_COUNT} > 0 ]] ; then echo "${YELLOW}${JOBS_COUNT} " ; fi)

	# different colors for super- and regular users
	local HOST_COL=$(if [[ ${UID} -eq "0" ]] ; then echo "${DRED}" ; else echo "${DCYAN}" ; fi)
	local PATH_COL=$(if [[ ${UID} -eq "0" ]] ; then echo "${RED}" ; else echo "${CYAN}" ; fi)

	local GIT_DESC="$(git desc 2>/dev/null| head -n1 | tr -d '\n')"
	if [[ "$GIT_DESC" != "" ]] ; then
		local GIT_DESC=" $GIT_DESC"
	fi
	local GIT_PMT=$([[ $(type __git_ps1 2>/dev/null) ]] && __git_ps1 "${CYAN} (${DRED}%s${RED}${GIT_DESC}${CYAN})")

	PS1="${CODE}${JOBS}${HOST_COL}\u@\h ${PATH_COL}\W${GIT_PMT}\n${SUCC_COL}\\$ ${RESET}"
}
PS2="\[\e[1;92m\]>\[\e[0m\] "

# careful resize for terminal emulators
shopt -s checkwinsize

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
# if ! shopt -oq posix; then
#   if [ -f /usr/share/bash-completion/bash_completion ]; then
#     . /usr/share/bash-completion/bash_completion
#   elif [ -f /etc/bash_completion ]; then
#     . /etc/bash_completion
#   fi
# fi
[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"

source ~/.bash_aliases
source ~/.bash_env

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Maven aliases
alias use-mvn-32='brew unlink maven@3.2 maven@3.3 && brew link maven@3.2 --force && mvn -version'
alias use-mvn-33='brew unlink maven@3.2 maven@3.3 && brew link maven@3.3 --force && mvn -version'
