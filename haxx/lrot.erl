-module(lrot).
-compile([export_all]).

head(XS, N) -> head(XS, N, []).
head([], _, YS) -> YS;
head(_, 0, YS) -> YS;
head([X|XS], N, YS) -> head(XS, N - 1, YS ++ [X]).

lrot(XS, N) ->
    EffN = N rem length(XS),
    
