-module(hglass).
-compile([export_all]).
-import(lists, [nth/2, sublist/3, sum/1, seq/2, max/1]).

hg(XSS, {X, Y}) ->
    sum(sublist(nth(Y, XSS), X, 3)
        ++ [nth(X+1, nth(Y+1, XSS))]
        ++ sublist(nth(Y+2, XSS), X, 3)).

hgs_pos([XS|XSS]) ->
    [{X, Y} || X <- seq(1, length(XS) - 2), Y <- seq(1, length(XSS) - 1)].

hourglassSum(XSS) ->
    max([hg(XSS, P) || P <- hgs_pos(XSS)]).
