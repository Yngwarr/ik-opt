-module(cloud).
-compile([export_all]).

cl([], N) -> N;
cl([0], N) -> N;
cl([_, 0], N) -> N + 1;
cl([_, _, 0|CS], N) -> cl([0|CS], N + 1);
cl([_, 0, 1|CS], N) -> cl([0, 1|CS], N + 1).

jumpingOnClouds(C) ->
    cl(C, 0).
