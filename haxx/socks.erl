-module(socks).
-compile([export_all]).

sm([], _, N) -> N;
sm([X|XS], Found, N) ->
    case lists:member(X, Found) of
        true -> sm(XS, Found -- [X], N+1);
        _ -> sm(XS, [X|Found], N)
    end.

% Complete the sockMerchant function below.
sockMerchant(_N, Ar) ->
    sm(Ar, [], 0).
