-module(rstring).
-compile([export_all]).

num_of(Y, XS) ->
    lists:foldl(
      fun (X, Acc) ->
              if X =:= Y -> Acc + 1;
                 true -> Acc
              end
      end, 0, XS).

head(XS, N) -> head(XS, N, []).
head([], _, YS) -> YS;
head(_, 0, YS) -> YS;
head([X|XS], N, YS) -> head(XS, N - 1, YS ++ [X]).

rs(XS, N) ->
    Nsubs = N div length(XS),
    Sub = head(XS, N rem length(XS)),
    Nsubs * num_of(97, XS) + num_of(97, Sub).
